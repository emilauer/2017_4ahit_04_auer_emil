﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQ_TO_XML
{
    class Program
    {
        static void Main(string[] args)
        {
                string myXML = @"<Departments>
                       <Department>Account</Department>
                       <Department>Sales</Department>
                       <Department>Pre-Sales</Department>
                       <Department>Marketing</Department>
                       </Departments>";

                XDocument xdoc = new XDocument();
                xdoc = XDocument.Parse(myXML);

                var result = xdoc.Element("Departments").Descendants();

                foreach (XElement item in result)
                {
                    Console.WriteLine("Department Name - " + item.Value);
                }

                Console.WriteLine("\nPress any key to continue.");
                Console.ReadKey();

            string myXML2 = @"<Departments>
                       <Department>Account</Department>
                       <Department>Sales</Department>
                       <Department>Pre-Sales</Department>
                       <Department>Marketing</Department>
                       </Departments>";

            XDocument xdoc2 = new XDocument();
            xdoc2 = XDocument.Parse(myXML2);

            //Add new Element
            xdoc2.Element("Departments").Add(new XElement("Department", "Finance"));

            //Add new Element at First
            xdoc2.Element("Departments").AddFirst(new XElement("Department", "Support"));

            var result2 = xdoc2.Element("Departments").Descendants();

            foreach (XElement item in result)
            {
                Console.WriteLine("Department Name - " + item.Value);
            }

            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();




            string myXML3 = @"<Departments>
                       <Department>Support</Department>
                       <Department>Account</Department>
                       <Department>Sales</Department>
                       <Department>Pre-Sales</Department>
                       <Department>Marketing</Department>
                       <Department>Finance</Department>
                       </Departments>";

            XDocument xdoc3 = new XDocument();
            xdoc3 = XDocument.Parse(myXML3);

            //Remove Sales Department
            xdoc3.Descendants().Where(s => s.Value == "Sales").Remove();

            var result3 = xdoc3.Element("Departments").Descendants();

            foreach (XElement item in result)
            {
                Console.WriteLine("Department Name - " + item.Value);
            }

            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }
        }
    }
