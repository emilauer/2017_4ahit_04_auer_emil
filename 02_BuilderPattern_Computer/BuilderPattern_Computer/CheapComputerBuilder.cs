﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class CheapComputerBuilder : IComputerBuilder
    {
        private Computer pc = new Computer();

        public void BuildHardDisk()
        {
            pc.AddPart(new CheapHardDisk());
        }

        public void BuildMonitor()
        {
            pc.AddPart(new CheapMonitor());
        }

        public void BuildProcessor()
        {
            pc.AddPart(new CheapProcessor());
        }

        public void BuildRam()
        {
            pc.AddPart(new CheapRam());
        }

        public Computer GetComputer()
        {
            return pc;
        }
    }
}
