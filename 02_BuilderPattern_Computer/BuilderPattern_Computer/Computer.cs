﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class Computer
    {
        private List<IPart> parts = new List<IPart>();

        public void AddPart (IPart part)
        {
            parts.Add(part);
        }

        public void Show()
        {
            Console.WriteLine("Computer Parts\n--------");
            foreach (IPart part in parts)
            {
                Console.WriteLine("\n" + part.ToString());
            }

            Console.WriteLine("\n--------\n\n");
        }
    }
}
