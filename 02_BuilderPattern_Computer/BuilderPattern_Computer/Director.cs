﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class Director
    {
        public void Construct(IComputerBuilder pcBuilder)
        {
            pcBuilder.BuildHardDisk();
            pcBuilder.BuildMonitor();
            pcBuilder.BuildProcessor();
            pcBuilder.BuildRam();
        }
    }
}
