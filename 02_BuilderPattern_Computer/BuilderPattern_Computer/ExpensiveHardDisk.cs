﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class ExpensiveHardDisk : IHardDisk
    {
        public override string ToString()
        {
            return "Expensive Harddisk";
        }
    }

    class CheapHardDisk : IHardDisk
    {
        public override string ToString()
        {
            return "Cheap Harddisk";
        }
    }
}
