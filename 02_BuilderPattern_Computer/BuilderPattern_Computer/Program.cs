﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class Program
    {
        static void Main(string[] args)
        {
            Director director = new Director();

            IComputerBuilder builder1 = new ExpensiveComputerBuilder();
            IComputerBuilder builder2 = new CheapComputerBuilder();
            IComputerBuilder builder3 = new MemoryComputerBuilder();

            director.Construct(builder1);
            director.Construct(builder2);
            director.Construct(builder3);

            Computer pc1 = builder1.GetComputer();
            Computer pc2 = builder2.GetComputer();
            Computer pc3 = builder3.GetComputer();

            pc1.Show();
            pc2.Show();
            pc3.Show();

            Console.ReadLine();
        }
    }
}
