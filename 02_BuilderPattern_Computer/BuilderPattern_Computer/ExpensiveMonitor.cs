﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class ExpensiveMonitor : IMonitor
    {
        public override string ToString()
        {
            return "Expensive Monitor";
        }
    }

    class CheapMonitor : IMonitor
    {
        public override string ToString()
        {
            return "Cheap Monitor";
        }
    }
}
