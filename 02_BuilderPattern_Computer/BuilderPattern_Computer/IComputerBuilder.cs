﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    interface IComputerBuilder
    {
        void BuildProcessor();
        void BuildRam();
        void BuildMonitor();
        void BuildHardDisk();

        Computer GetComputer();
    }
}
