﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class ExpensiveProcessor : IProcessor
    {
        public override string ToString()
        {
            return "Expensive Processor";
        }
    }

    class CheapProcessor : IProcessor
    {
        public override string ToString()
        {
            return "Cheap Processor";
        }
    }
}
