﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    interface IProcessor : IPart
    {
        string ToString();
    }
}
