﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class ExpensiveRam : IRam
    {
        public override string ToString()
        {
            return "Expensive Ram";
        }
    }

    class CheapRam : IRam
    {
        public override string ToString()
        {
            return "Cheap Ram";
        }
    }
}
