﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern_Computer
{
    class MemoryComputerBuilder : IComputerBuilder
    {
        private Computer pc = new Computer();

        public void BuildHardDisk()
        {
            pc.AddPart(new ExpensiveHardDisk());
        }

        public void BuildMonitor()
        {
            pc.AddPart(new CheapMonitor());
        }

        public void BuildProcessor()
        {
            pc.AddPart(new CheapProcessor());
        }

        public void BuildRam()
        {
            pc.AddPart(new ExpensiveRam());
        }

        public Computer GetComputer()
        {
            return pc;
        }
    }
}
