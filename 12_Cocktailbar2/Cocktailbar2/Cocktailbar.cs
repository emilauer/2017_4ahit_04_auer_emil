﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Cocktailbar2
{
    public class Cocktailbar
    {
        static SemaphoreSlim s_c = new SemaphoreSlim(0);
        static SemaphoreSlim s_m = new SemaphoreSlim(1);

        public void TesteWorkerThread()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread t = new Thread(Serve);
                t.Name = "Kellner " + i;
                t.Start();
            }
            Console.WriteLine("Besprechung");
            s_c.Release(3);
        }
        private static void Serve()
        {
                s_c.Wait();
                Console.WriteLine("{0} betritt die Bar", Thread.CurrentThread.Name);
                MixCocktail();
                Console.WriteLine("{0} verlässt die Bar", Thread.CurrentThread.Name);
                s_c.Release();
        }
        private static void MixCocktail()
        {
            s_m.Wait();
            Console.WriteLine("{0} nutzt den Mixer", Thread.CurrentThread.Name);
            Console.WriteLine("{0} gibt den Mixer frei", Thread.CurrentThread.Name);
            s_m.Release();
        }
    }
}
