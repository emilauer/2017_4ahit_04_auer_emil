﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace Apfelbauer
{
    class Simulator
    {
        private List<Apple> apples;

        public List<Apple> Apples
        {
            get
            {
                return apples;
            }

            set
            {
                apples = value;
            }
        }

        public void Simulate()
        {
            Apples = Applefarmer.generateApples();
        }
        public List<Apple> sortApples(List<Apple> unsortedApples, Func<Apple, bool> sortCondition)
        {
            List<Apple> sortedApples = new List<Apple>();
            foreach (var item in unsortedApples)
            {
                if (sortCondition(item))
                    sortedApples.Add(item);
            }
            return sortedApples;
        }
    }

}
