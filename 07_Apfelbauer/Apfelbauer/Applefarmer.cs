﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;

namespace Apfelbauer
{

    public enum color { green, red, yellow }

    public class Apple
    {
        public color Color { get; set; }

        public override string ToString()
        {
            return "Apfel " + Color;
        }
    }


    class Applefarmer
    {
        private static List<Apple> apples = new List<Apple>();
        private static Apple apple;
        static Random rnd = new Random();

        public static List<Apple> generateApples()
        {
            for (int i = 0; i < 50; i++)
            {
                switch (rnd.Next(3))
                {
                    case 1:
                        apple = new Apple();
                        apple.Color = color.green;
                        apples.Add(apple);
                        break;
                    case 2:
                        apple = new Apple();
                        apple.Color = color.red;
                        apples.Add(apple);
                        break;
                    case 3:
                        apple = new Apple();
                        apple.Color = color.yellow;
                        apples.Add(apple);
                        break;
                }
            }
            return apples;
        
        }

        public bool isRed(Apple a)
        {
            if (a.Color == color.red)
                return true;

            return false;
        }

        public bool isYellow(Apple a)
        {
            if (a.Color == color.yellow)
                return true;

            return false;
        }

        public bool isGreen(Apple a)
        {
            if (a.Color == color.green)
                return true;

            return false;
        }

    }

}
