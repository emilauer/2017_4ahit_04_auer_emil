﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apfelbauer
{


    class Program
    {
        static void Main(string[] args)
        {
            
            Applefarmer af = new Applefarmer();
            Simulator s = new Simulator();
            Apple ap = new Apple();
            ap.Color = color.green;

            Func<Apple, bool> function = af.isGreen;
            Func<Apple, bool> function1 = af.isRed;
            Func<Apple, bool> function2 = af.isYellow;

            s.Simulate();
            foreach (Apple a in s.Apples)
            {
                Console.WriteLine(a.ToString());
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            List<Apple> sorted = s.sortApples(s.Apples, function1);
            foreach (Apple a in sorted)
            {
                Console.WriteLine(a.ToString());
            }

            Console.ReadLine();
        }
    }
}
