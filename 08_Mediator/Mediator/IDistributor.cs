﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
   public interface IDistributor
    {
        void Send(Person from, Person to, string message);
    }
}
