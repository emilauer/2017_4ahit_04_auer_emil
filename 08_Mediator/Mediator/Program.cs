﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            IDistributor distributor = new ConcreteDistributor();

            Person schandl = new ConcretePerson("Max", distributor);
            Person huhitzer = new ConcretePerson("Hans", distributor);
            Person naber = new ConcretePerson("Franz", distributor);

            schandl.Send(huhitzer, "Servas");
            huhitzer.Send(schandl, "Griaß di");
            schandl.Send(naber, "Servas");
            naber.Send(schandl, "Pfiad di");

            foreach (string m in naber.chat) 
            {
                Console.WriteLine(m);
            }
            Console.ReadKey();
        }
    }
}
