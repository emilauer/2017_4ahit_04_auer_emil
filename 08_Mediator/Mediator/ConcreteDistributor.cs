﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class ConcreteDistributor : IDistributor
    {
        public void Send(Person from, Person to, string message) 
        {
            if (from != null && to != null)
            {
                to.Notify(from, message);
            }
        }
    }
}
