﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProducerConsumer
{
    class Pipe
    {
        private static BlockingCollection<Calculate> collection = new BlockingCollection<Calculate>();

        private static Pipe pipe;

        private Pipe()
        {}

        public BlockingCollection<Calculate> getCollection()
        {
            return collection;
        }

        public static Pipe getPipe()
        {
            if (pipe == null)
                pipe = new Pipe();
            return pipe;
        }

        public void Add(Calculate c)
        {
            collection.Add(c);
        }
    }


    //class Pipe : Queue<Calculate>
    //{
    //    private static Pipe pipe;

    //    private Pipe()
    //    { }

    //    public static Pipe getPipe()
    //    {
    //        if (pipe == null)
    //            pipe = new Pipe();
    //        return pipe;
    //    }

    //    public void Add(Calculate c)
    //    {
    //        pipe.Enqueue(c);
    //    }
    //}
}
