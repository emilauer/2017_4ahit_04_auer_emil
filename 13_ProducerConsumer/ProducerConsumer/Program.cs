﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ProducerConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Producer p = new Producer();
            Consumer c = new Consumer();
            Thread t = new Thread(p.execute);
            Thread t2 = new Thread(c.execute);
            t.Start();
            Thread.Sleep(100);
            t2.Start();

            Console.ReadLine();
        }
    }
}
