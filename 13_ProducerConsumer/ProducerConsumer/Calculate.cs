﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProducerConsumer
{
    class Calculate
    {
        static int count = 1;
        int rand1;
        int rand2;
        int id;
        Random rand = new Random();

        public Calculate()
        {
            Random rand = new Random();
            rand1 = rand.Next(0,100);
            rand2 = rand.Next(0, 100);
            id = count;
            count++;
        }

        public int execute()
        {
            return rand1 * rand2;
        }

        public override string ToString()
        {
            return "Calculate " + id + ": " + rand1 + " * " + rand2 + " = " + execute();
        }
    }
}
