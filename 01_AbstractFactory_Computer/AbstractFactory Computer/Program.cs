﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory_Computer
{
    class Program
    {
        static void Main(string[] args)
        {
            IComputer pc = new MemoryComputer();
            pc.buildHardDisk();
            pc.buildMonitor();
            pc.buildProcessor();
            pc.buildRam();
            Console.WriteLine(pc.ToString());

            Console.WriteLine();

            IComputer pc2 = new ExpensiveComputer();
            pc2.buildHardDisk();
            pc2.buildMonitor();
            pc2.buildProcessor();
            pc2.buildRam();
            Console.WriteLine(pc2.ToString());

            Console.WriteLine();

            IComputer pc3 = new CheapComputer();
            pc3.buildHardDisk();
            pc3.buildMonitor();
            pc3.buildProcessor();
            pc3.buildRam();
            Console.WriteLine(pc3.ToString());

            Console.ReadLine();
        }
    }
}
