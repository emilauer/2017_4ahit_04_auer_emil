﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory_Computer
{
    class CheapComputer : IComputer
    {
        IHardDisk harddisk;
        IMonitor monitor;
        IProcessor processor;
        IRam ram;

        public IHardDisk buildHardDisk()
        {
            harddisk = new CheapHardDisk();
            return harddisk;
        }

        public IMonitor buildMonitor()
        {
            monitor = new CheapMonitor();
            return monitor;
        }

        public IProcessor buildProcessor()
        {
            processor = new CheapProcessor();
            return processor;
        }

        public IRam buildRam()
        {
            ram = new CheapRam();
            return ram;
        }

        public override string ToString()
        {
            return "CheapComputer\n ----------- \n" + processor.ToString() + "\n" + harddisk.ToString() + "\n" + ram.ToString() + "\n" + monitor.ToString();
        }
    }
}
