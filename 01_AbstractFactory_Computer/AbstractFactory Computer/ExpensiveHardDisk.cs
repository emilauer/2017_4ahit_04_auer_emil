﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory_Computer
{
    class ExpensiveHardDisk : IHardDisk
    {
        public override string ToString()
        {
            return "ExpensiveHardDisk";
        }
    }
}
