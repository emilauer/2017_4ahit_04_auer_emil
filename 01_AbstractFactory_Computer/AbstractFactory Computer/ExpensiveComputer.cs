﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory_Computer
{
    class ExpensiveComputer : IComputer
    {
        IHardDisk harddisk;
        IMonitor monitor;
        IProcessor processor;
        IRam ram;

        public IHardDisk buildHardDisk()
        {
            harddisk = new ExpensiveHardDisk();
            return harddisk;
        }

        public IMonitor buildMonitor()
        {
            monitor = new ExpensiveMonitor();
            return monitor;
        }

        public IProcessor buildProcessor()
        {
            processor = new ExpensiveProcessor();
            return processor;
        }

        public IRam buildRam()
        {
            ram = new ExpensiveRam();
            return ram;
        }

        public override string ToString()
        {
            return "ExpensiveComputer\n-----------\n" + processor.ToString() + "\n" + harddisk.ToString() + "\n" + ram.ToString() + "\n" + monitor.ToString();
        }
    }
}
