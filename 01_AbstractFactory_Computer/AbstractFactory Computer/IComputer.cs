﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory_Computer
{
    interface IComputer
    {
        IProcessor buildProcessor();
        IMonitor buildMonitor();
        IHardDisk buildHardDisk();
        IRam buildRam();
    }
}
