﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory_Computer
{
    class ExpensiveMonitor : IMonitor
    {
        public override string ToString()
        {
            return "ExpensiveMonitor";
        }
    }
}
