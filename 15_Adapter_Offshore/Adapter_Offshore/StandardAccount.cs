﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Offshore
{
    public class StandardAccount:AbstractAccount
    {
        public StandardAccount(double balance):base(balance)
        {
            setOverdraftAvailable(false);
        }
    }
}
