﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Offshore
{
    public class PlatinumAccount:AbstractAccount
    {
        public PlatinumAccount(double balance):base(balance)
        {
            setOverdraftAvailable(true);
        }
    }
}
