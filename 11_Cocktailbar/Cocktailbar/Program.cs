﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Cocktailbar
{
    class Program
    {
        static SemaphoreSlim sem_c = new SemaphoreSlim(3);

        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                Thread t = new Thread(CreateCocktail);
                t.Name = "Kellner " + i;
                t.Start();
            }
            Console.ReadLine();
        }

        private static void CreateCocktail()
        {
            sem_c.Wait();
            Console.WriteLine(Thread.CurrentThread.Name + " betritt die Bar");
            MixCocktail();
        }
        private static void MixCocktail()
        {
            Console.WriteLine(Thread.CurrentThread.Name + " mixt Cocktail");
            Thread.Sleep(200);
            Console.WriteLine(Thread.CurrentThread.Name + " verlässt die Bar");
            sem_c.Release();
        }
    }
}
