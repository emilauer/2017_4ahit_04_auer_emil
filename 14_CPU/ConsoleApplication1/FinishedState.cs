﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class FinishedState : IState
    {
        public IState changeState(MyThread mt)
        {
            Console.WriteLine(toString() + " " + mt.getName());

            return this;
        }

        public string toString()
        {
            return "Finished state:";
        }
    }
}
