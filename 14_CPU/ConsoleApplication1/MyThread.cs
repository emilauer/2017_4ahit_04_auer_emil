﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class MyThread
    {
        IState state;
        string name;

        public string getName()
        {
            return name;
        }

        public IState getState()
        {
            return state;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public void setState(IState state)
        {
            this.state = state;
        }

        public void nextState()
        {
            state = state.changeState(this);
        }

        public override string ToString()
        {
            return name + ": " + state.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;
            return false;
        }
    }
}
