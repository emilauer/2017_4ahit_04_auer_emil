﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class ReadyQueueCustomer
    {
        public void run()
        {
            while (true)
            {
                MyThread m = new MyThread();
                if (ReadyQueue.getInstance().TryTake(out m))
                {
                    m.nextState();
                    RunningQueue.getInstance().TryAdd(m);
                }
            }
        }
    }
}
