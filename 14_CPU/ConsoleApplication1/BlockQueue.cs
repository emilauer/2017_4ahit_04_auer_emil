﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ConsoleApplication1
{
    class BlockQueue : BlockingCollection<MyThread>
    {
        private static BlockQueue ourInstance;

        public BlockQueue()
        {

        }

        public static BlockQueue getInstance()
        {
            if (ourInstance == null)
                ourInstance = new BlockQueue();
            return ourInstance;
        }
    }
}
