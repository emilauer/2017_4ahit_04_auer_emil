﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <=5; i++)
			{
                MyThread mt = new MyThread();
                mt.setName("Thread " + i);
                mt.setState(new ReadyState());
                ReadyQueue.getInstance().TryAdd(mt);
			}

            ReadyQueueCustomer rq = new ReadyQueueCustomer();
            BlockQueueCustomer bq = new BlockQueueCustomer();
            RunningQueueCustomer rqc = new RunningQueueCustomer();

            Task t1 = new Task(rq.run);
            Task t2 = new Task(bq.run);
            Task t3 = new Task(rqc.run);

            t1.Start();
            t2.Start();
            t3.Start();

            Console.ReadLine();
        }
    }
    
}
