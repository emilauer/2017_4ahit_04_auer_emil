﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPatternDemo
{
    class Computer : IComputerVisitor
    {

        public void build(List<IComputerPart> parts)
        {
            Console.WriteLine("Computer\n--------");
            foreach (IComputerPart part in parts)
            {
                part.implement(this);
            }
        }

        public void visit(IComputerPart part)
        {
            Console.WriteLine(part.ToString());
        }
    }
}
