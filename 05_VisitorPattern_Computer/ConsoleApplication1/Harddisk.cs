﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPatternDemo
{
    class Harddisk : IComputerPart
    {
        public void implement(IComputerVisitor cpv)
        {
            cpv.visit(this);
        }

        public override string ToString()
        {
            return "Harddisk";
        }
    }
}
