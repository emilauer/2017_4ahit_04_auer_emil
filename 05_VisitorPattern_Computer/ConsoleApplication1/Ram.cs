﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPatternDemo
{
    class Ram : IComputerPart
    {
        public void implement(IComputerVisitor cpv)
        {
            cpv.visit(this);
        }

        public override string ToString()
        {
            return "Ram";
        }
    }
}
