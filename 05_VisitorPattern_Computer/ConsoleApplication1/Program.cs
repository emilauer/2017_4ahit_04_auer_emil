﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPatternDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IComputerPart> parts = new List<IComputerPart>();
            parts.Add(new Harddisk());
            parts.Add(new Monitor());
            parts.Add(new Processor());
            parts.Add(new Ram());

            IComputerVisitor computer = new Computer();

            computer.build(parts);

            Console.ReadLine();
        }
    }
}
