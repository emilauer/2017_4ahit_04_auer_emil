﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorPatternDemo
{
    interface IComputerVisitor
    {
        void visit(IComputerPart part);

        void build(List<IComputerPart> parts);
    }
}
