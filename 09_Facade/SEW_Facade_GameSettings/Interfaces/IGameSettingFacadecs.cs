﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEW_Facade_GameSettings
{
    interface IGameSettingFacade
    {
        void SetGraphicSettings(GraphicsPreferences g);
        void SetSoundSettings(SoundPreferences s);
        void SetGeneralSettings(GamePreferences g, GeneralPreferences ge);
    }

    class DetailedGameSettingsFacade:IGameSettingFacade
    {
        IGraphics gameGraphics;
        ISoundSetting gameSoundSettings;
        IGameGeneralSettingFacade gameGeneralSettings;

        public DetailedGameSettingsFacade(IGraphics g, ISoundSetting s, IGameGeneralSettingFacade gameGeneralSettings)
        {
            gameGraphics = g;
            gameSoundSettings = s;
            this.gameGeneralSettings = gameGeneralSettings;
        }

        public void SetGeneralSettings(GamePreferences g, GeneralPreferences ge)
        {
            gameGeneralSettings.Configure(ge, g);
        }

        public void SetGraphicSettings(GraphicsPreferences g)
        {
            gameGraphics.SetResolution(g.yResolution, g.xResolution);
            gameGraphics.SetFPS(g.fps);
            gameGraphics.SetShadowQuality(g.shadowQuality);
            gameGraphics.SetWaterQuality(g.waterQuality);
            gameGraphics.TextureQuality(g.textureQuality);
            gameGraphics.TestIt();
        }

        public void SetSoundSettings(SoundPreferences s)
        {
            gameSoundSettings.SetDialogueSound(s.dialogueSound);
            gameSoundSettings.SetEnvironmentSound(s.environmentSound);
            gameSoundSettings.SetSoundEffects(s.soundEffects);
            gameSoundSettings.SetTheGeneralSound(s.generalSound);
            gameSoundSettings.TestIt();
        }
    }
}
